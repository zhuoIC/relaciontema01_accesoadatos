# README #

Relación de ejercicios del Tema 01 de la asignatura Acceso a Datos.

### Lista de ejercicios ###

* Ejercicio 1: Realiza la conversión de divisas a euros/dólares respecto al cuadro de texto de la derecha.
* Ejercicio 2: Abre la dirección web escrita en otra activity diferente de forma embebida en la aplicación.
* Ejercicio 3: Es un temporizador que se le puede aumentar y disminuir los minutos, cada vez que finaliza aumenta en 1 el número de cafés. Cuando se llega a 10 cafés hay que resetear para poder seguir usándolo.

### Ejercicio 4 ###

Se basa en el juego de dados **Craps**. Se tiran dos dados y según el valor de la suma de los dados se gana, se pierde 
o se sigue jugando:

* En la primera tirada se pude ganar si se saca un **7** o un **11**. Si se saca un **2**, un **3** o un **12** se pierde.
* Si se saca otro valor se obtiene el **punto**: se sigue jugando hasta volver a sacar **ese numero** (se gana) o el **7** (se pierde).

Funcionalidades añadidas:

1. Al presionar el botón Tirar Dados las imagenes de los dados se actualizan con el valor de la tirada correspondiente al dado.
2. Cada vez que se realiza una tirada se reproduce un sonido que simula la tirada de dados.
3. Cuando se acaba la partida, tanto si se gana como si se pierde, se muestra una ventana emergente con un mensaje avisando de ello.

### Extras ###

Ejercicio3:

* Botón Temporizador/Cronómetro: Si se pulsa el botón Temporizador se puede alternar entre el uso como temporizador o como cronómetro.

### Autor ###

* Nicolás Hernández Jiménez
package com.example.nico.relaciontema01;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;

/**
 * Abre el Intent con la dirección web correspondiente
 * @author Nicolás Hernández Jiménez
 */
public class NavegatorOpenedActivity extends AppCompatActivity {
    public static final String KEY = "URL";
    private WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navegator_opened);
        webView = (WebView) findViewById(R.id.webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.loadUrl(getIntent().getExtras().getString(KEY));
    }
}

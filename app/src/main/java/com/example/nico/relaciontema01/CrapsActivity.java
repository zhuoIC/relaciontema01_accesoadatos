package com.example.nico.relaciontema01;

import android.media.MediaPlayer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nico.relaciontema01.pojo.Dado;

import java.io.IOException;

/**
 * Juego de dados en el que se puede ganar o perder dependiendo del resultado de las tiradas.
 * @author Nicolás Hernández Jiménez
 */
public class CrapsActivity extends AppCompatActivity {
    private TextView txvPunto;
    private TextView txvResultado;
    private Button btnTirar;
    private ImageView imgDado1;
    private ImageView imgDado2;
    private GestionDado gestionDado;
    private AlertDialog.Builder popup;
    private MediaPlayer mp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_craps);
        txvPunto = (TextView) findViewById(R.id.txvPunto);
        txvResultado = (TextView) findViewById(R.id.txvResultado);
        btnTirar = (Button) findViewById(R.id.btnTirar);
        imgDado1 = (ImageView) findViewById(R.id.imgDado1);
        imgDado2 = (ImageView) findViewById(R.id.imgDado2);
        gestionDado = new GestionDado();
        popup  = new AlertDialog.Builder(CrapsActivity.this);
        mp = MediaPlayer.create(this, R.raw.rolldice);
        btnTirar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int resultado = 0;
                int sumaDados = gestionDado.tirarDados();
                if (mp.isPlaying()){
                    mp.stop();
                    try {
                        mp.prepare();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                mp.start();
                txvResultado.setText("Resultado: " + sumaDados);
                if((resultado = gestionDado.comprobarResultado(sumaDados)) != -1){
                    if (resultado == 1) {
                        txvPunto.setText("Tu punto: " + gestionDado._punto);
                        popup.setTitle("¡Has ganado!");
                        popup.setMessage("Enhorabuena.");
                    }
                    else {
                        txvPunto.setText("Tu punto: ---");
                        popup.setTitle("Has perdido...");
                        popup.setMessage("Lo sentimos");
                    }
                    popup.setPositiveButton("Ok", null);
                    popup.show();
                    gestionDado._punto = -1;
                }
                else {
                    txvPunto.setText("Tu punto: " + gestionDado._punto);
                }
            }
        });
    }

    public class GestionDado {
        private Dado dado1 = new Dado(6);
        private Dado dado2 = new Dado(6);
        private int _punto = -1;

        private int comprobarResultado(int sumaDados) {
            int esVictoria = -1;
            if (_punto == -1) {
                if (sumaDados == 7 || sumaDados == 11) {
                    esVictoria = 1;
                }
                else if (sumaDados == 2 || sumaDados == 3 || sumaDados == 12) {
                    esVictoria = 0;
                }
                _punto = sumaDados;
            }
            else {
                if (sumaDados == 7) {
                    esVictoria = 0;
                } else if (_punto == sumaDados) {
                    esVictoria = 1;
                }
            }
            return esVictoria;
        }

        private int tirarDados(){
            imgDado1.setImageResource(obtenerImagen(dado1.tirarDado()));
            imgDado2.setImageResource(obtenerImagen(dado2.tirarDado()));
            return dado1.ultTirada + dado2.ultTirada;
        }

        private int obtenerImagen(int resultado){
            int idImagen = 0;
            switch (resultado){
                case 1:
                    idImagen = R.drawable.dado1;
                    break;
                case 2:
                    idImagen = R.drawable.dado2;
                    break;
                case 3:
                    idImagen = R.drawable.dado3;
                    break;
                case 4:
                    idImagen = R.drawable.dado4;
                    break;
                case 5:
                    idImagen = R.drawable.dado5;
                    break;
                case 6:
                    idImagen = R.drawable.dado6;
                    break;
            }
            return idImagen;
        }
    }
}
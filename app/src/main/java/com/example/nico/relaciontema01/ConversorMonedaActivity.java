package com.example.nico.relaciontema01;

import android.content.Intent;
import android.renderscript.Double2;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

/**
 * Realiza la conversión de divisas a euros/dólares respecto al cuadro de texto de la derecha.
 * @author Nicolás Hernández Jiménez
 */
public class ConversorMonedaActivity extends AppCompatActivity implements View.OnClickListener{

    private double getCambio;
    private EditText etxE;
    private EditText etxD;
    private Button btnConvertir;
    private RadioButton rbtEaD;
    private ViewGroup layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversor_moneda);
        etxE = (EditText) findViewById(R.id.etxE);
        etxD = (EditText) findViewById(R.id.etxD);
        btnConvertir = (Button) findViewById(R.id.btnConvertir);
        rbtEaD = (RadioButton) findViewById(R.id.rbtnE);
        layout = (LinearLayout) findViewById(R.id.layout);

        btnConvertir.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == btnConvertir) {
            actualizar();
        }
    }

    // Realiza el cambio a dólares/euros y lo muestra con dos decimales
    public void actualizar() {
        double valor = 0;

        try {
            getCambio = Double.parseDouble(((EditText) findViewById(R.id.edtGetCambio)).getText().toString());
            if (rbtEaD.isChecked()) {
                etxD.setText(String.format("%.2f", Double.parseDouble(etxE.getText().toString()) * getCambio));
            } else {

                etxE.setText(String.format("%.2f", Double.parseDouble(etxD.getText().toString()) / getCambio));
            }
        } catch (Exception e) {
            // Muestra en una barra inferior el mensaje de error
            Snackbar.make(layout, "Error al introducir datos", Snackbar.LENGTH_SHORT).show();
        }
    }

}
package com.example.nico.relaciontema01;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.Button;

/**
 * Relación de ejercicios del Tema 1 de la asignatura Acceso a Datos
 * @author Nicolás Hernández Jiménez
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private Button btnE1, btnE2, btnE3, btnE4;
    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_linear);
        btnE1 = (Button) findViewById(R.id.btn1);
        btnE2 = (Button) findViewById(R.id.btn2);
        btnE3 = (Button) findViewById(R.id.btn3);
        btnE4 = (Button) findViewById(R.id.btn4);
        btnE1.setOnClickListener(this);
        btnE2.setOnClickListener(this);
        btnE3.setOnClickListener(this);
        btnE4.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btn1:
                intent = new Intent(MainActivity.this, ConversorMonedaActivity.class);
                startActivity(intent);
                break;
            case R.id.btn2:
                intent = new Intent(MainActivity.this, OpenNavegatorActivity.class);
                startActivity(intent);
                break;
            case R.id.btn3:
                intent = new Intent(MainActivity.this, ContadorCafesActivity.class);
                startActivity(intent);
                break;
            case R.id.btn4:
                intent = new Intent(MainActivity.this, CrapsActivity.class);
                startActivity(intent);
                break;
        }
    }
}

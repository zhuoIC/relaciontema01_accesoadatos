package com.example.nico.relaciontema01.pojo;

import java.util.Random;

/**
 * Created by nico on 8/10/17.
 */

public class Dado {
    private int lados;
    private Random random = new Random();
    public int ultTirada;

    public Dado(int lados) {
        this.lados = lados;
    }

    public int getLados() {
        return lados;
    }

    public int tirarDado() {
        ultTirada =  (random.nextInt(lados) + 1);
        return ultTirada;
    }
}

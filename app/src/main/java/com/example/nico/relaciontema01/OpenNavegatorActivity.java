package com.example.nico.relaciontema01;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Abre el navegador en la otra Activity. Permite editar la dirección web a inyectar.
 * @author Nicolás Hernández Jiménez
 */
public class OpenNavegatorActivity extends AppCompatActivity {
    public static final String KEY = "URL";
    private Intent intent;
    private Button btnIr;
    private EditText edtUrl;
    private String url;
    private final String PROTOCOLO = "https://";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_navegator);
        btnIr = (Button) findViewById(R.id.btnIr);
        edtUrl = (EditText) findViewById(R.id.edtUrl);
        btnIr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                // Obtengo el texto del TextView y hago un casting a uri
                Uri uri = Uri.parse(edtUrl.getText().toString());
                // Si es relativa le añade el protocolo https
                if (uri.isRelative()){
                    url = PROTOCOLO + uri.toString();
                }
                bundle.putString(KEY, url);
                intent = new Intent(OpenNavegatorActivity.this, NavegatorOpenedActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}

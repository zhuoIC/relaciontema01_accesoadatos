package com.example.nico.relaciontema01;

import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;

/**
 * Simula la función de un temporizador/cronómetro. Se puede programar el tiempo de inicio o bien el del final.
 * @author Nicolás Hernández Jiménez
 */
public class ContadorCafesActivity extends AppCompatActivity implements View.OnClickListener{
    private Button btnMas;
    private Button btnMenos;
    private Button btnComenzar;
    private Button btnReset;
    private Button btnCroAndCont;
    private long minutos;
    private long segundos;
    private TextView txvNumCafe;
    private TextView txvTemp;
    private ViewGroup llyPrincipal;
    private MyCountDownTimer miContador;
    private Snackbar snackbar;
    private MediaPlayer mp;
    private Byte numCafes;
    private AlertDialog.Builder popup;
    private final String TEMPORIZADOR = "Temporizador";
    private final String CRONO = "Cronómetro";
    private boolean isCrono = false;

    public void setCambio(boolean cambio) {
        if (cambio){
            btnCroAndCont.setText(CRONO);
        }
        else{
            btnCroAndCont.setText(TEMPORIZADOR);
        }
        this.isCrono = cambio;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contador_cafes);
        btnMas = (Button) findViewById(R.id.btnMas);
        btnMenos = (Button) findViewById(R.id.btnMenos);
        btnComenzar = (Button) findViewById(R.id.btnComenzar);
        btnReset = (Button) findViewById(R.id.btnReset);
        btnCroAndCont = (Button) findViewById(R.id.btnCroCont);
        txvNumCafe = (TextView) findViewById(R.id.txvNumCafe);
        txvTemp = (TextView) findViewById(R.id.txvTemp);
        llyPrincipal = (ViewGroup) findViewById(R.id.llyPrincipal);
        btnMas.setOnClickListener(this);
        btnMenos.setOnClickListener(this);
        btnComenzar.setOnClickListener(this);
        btnReset.setOnClickListener(this);
        btnCroAndCont.setOnClickListener(this);
        minutos = Long.parseLong((txvTemp.getText().toString()).substring(0,2));
        numCafes = Byte.parseByte(txvNumCafe.getText().toString());
        mp = MediaPlayer.create(this, R.raw.slurp);
    }

    @Override
    public void onClick(View view) {
        if (miContador == null ) {
            if(numCafes < 10) {
                switch (view.getId()) {
                    case (R.id.btnMas):
                        if (minutos < 60) {
                            minutos++;
                            PintarReloj();
                        }
                        break;
                    case (R.id.btnMenos):
                        if (minutos > 0) {
                            minutos--;
                            PintarReloj();
                        }
                        break;
                    case (R.id.btnComenzar):
                        miContador = new MyCountDownTimer(minutos * 60 * 1000, 1000, !isCrono);
                        miContador.start();
                        break;
                    case (R.id.btnCroCont):
                        setCambio(!isCrono);
                        break;
                }
            }
            else{
                if (view == btnReset){
                    txvNumCafe.setText("00");
                    numCafes = Byte.parseByte(txvNumCafe.getText().toString());
                }
            }
        }
    }

    View.OnClickListener snackbarClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            snackbar.dismiss();
        }
    };


    public class MyCountDownTimer extends CountDownTimer{

        private long starTime;
        private boolean cuentaAtras;
        public MyCountDownTimer(long startTime, long interval, boolean cuentaAtras) {
            super(startTime, interval);
            this.starTime = startTime;
            this.cuentaAtras = cuentaAtras;
        }

        public boolean isCuentaAtras() {
            return cuentaAtras;
        }

        public void setCuentaAtras(boolean cuentaAtras) {
            this.cuentaAtras = cuentaAtras;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (isCuentaAtras()){
                minutos = (millisUntilFinished / 1000 / 60);
                segundos = (millisUntilFinished / 1000) % 60;
            }
            else{
                minutos = ((starTime - millisUntilFinished) / 1000 / 60);
                segundos = ((starTime - millisUntilFinished) / 1000) % 60;
            }
            PintarReloj();
        }
        @Override
        public void
        onFinish() {
            ResetearReloj();
            txvNumCafe.setText(String.valueOf(++numCafes));
            mp.start();
            snackbar  = Snackbar.make(llyPrincipal, "Pausa terminada!!", Snackbar.LENGTH_LONG).setAction("Dismmiss", snackbarClickListener);
            snackbar.show();
            if (numCafes == 10){
                popup  = new AlertDialog.Builder(ContadorCafesActivity.this);
                popup.setTitle("¡Demasiados cafes!");
                popup.setMessage("Si quieres seguir tomando cafes dale al botón Reset.");
                popup.setPositiveButton("Ok", null);
                popup.show();
            }
        }
    }

    private void ResetearReloj() {
        miContador = null;
        minutos = Long.parseLong((txvTemp.getText().toString()).substring(0,2));
        segundos = 0;
        PintarReloj();
    }

    private void PintarReloj() {
        txvTemp.setText(String.format("%02d", minutos) + ":" + String.format("%02d", segundos));
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.onStop();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (miContador != null) {
            miContador.onFinish();
        }
        if (mp.isPlaying()){
            mp.stop();
            try {
                mp.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}